package youwen.tool;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则判断类
 * @author KK
 *
 */
public class RegularJudgment {
/*
	public static void main(String arg[]) {
		System.out.println(isPath("g:\\ff.json"));
	}***/
	
	/**
	 * 判断是否为合法IP 
	 * @param ipAddress 输入IP 
	 * @return
	 */
    public static boolean isboolIp(String ipAddress) {  
    	if(ipAddress!= null && ipAddress.length()>=7 && ipAddress.length()<=15) {
    		String ip = "([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}";   
            Pattern pattern = Pattern.compile(ip);  
            Matcher matcher = pattern.matcher(ipAddress);  
            return matcher.matches();  
    	}
        return false;
    } 
    /**
     * 判断是否为合法路径
     * @param PathAddress
     * @return
     */
    public static boolean isPath(String  PathAddress) {//"[a-zA-Z]:(\\\\([a-zA-Z0-9_]+.[a-zA-Z0-9_]{1,16}))+";
    	if(PathAddress != null && PathAddress.length() > 1) {
    		String Path = "\\w:\\\\[^<>,/,\\,|,:,\"\",*,? ]+(\\.[a-zA-Z0-9]+|$)";
    		Pattern pattern = Pattern.compile(Path);  
            Matcher matcher = pattern.matcher(PathAddress);  
            return matcher.matches();  
    	}
    	return false;
    }
    
    /**
     * 判断字符串是否是数值，整数和浮点数均可判断
     * @param Data
     * @return
     */
    public static boolean isdata(String Data) {
    	if(Data != null && Data.length()>0) {
    		String fDatama = "^(-?\\d+)(\\.\\d+)?$";//浮点数正则
        	Pattern patternf = Pattern.compile(fDatama);  
            Matcher matcherf = patternf.matcher(Data);
            
            String IDatama = "^-?\\d+$";//整数正则
            Pattern patternI = Pattern.compile(IDatama);  
            Matcher matcherI = patternI.matcher(Data);  
            if(matcherf.matches() || matcherI.matches()) {
            	return true;
            }
    	}
        return false;
    }
}
