package youwen.jiekou;

import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.Map;

public interface  TCPSercallBack {
	public void IniResult(boolean rs,Integer port);//tcpserver初始化结果
	public void Disconnected(SocketChannel SocketClient,String address);//连接已经断开
	public void MssageArrived(byte[] buffer,String address);//有数据到达
	public void Hasconnect(SocketChannel SocketClient);//连接上了
}
