package youwen.callback;

import java.io.UnsupportedEncodingException;
import java.util.Set;

import youwen.jiekou.TCPClientcallBack;

public final class TCPClientCallBack implements TCPClientcallBack {

	@Override
	public void Disconnected() {

		System.out.println("TCP连接断开");

	}

	@Override
	public void MssageArrived(byte readbuffer[], int Length) {

		System.out.println("TCP数据到达...");
		String MSG;
		try {
			MSG = new String(readbuffer, 0, Length, "utf-8");
			try {
				System.out.println(MSG.length() + ",MSG:" + MSG);
			} catch (Exception e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		} catch (Exception e1) {
			// TODO 自动生成的 catch 块
			e1.printStackTrace();
		}

		// TODO 自动生成的方法存根
	}

	@Override
	public void Hasconnect() {
		System.out.println("TCP连接成功");
		// TODO 自动生成的方法存根
	}

	@Override
	public void ConnectErro() {
		System.out.println("TCP连接失败");
	}

}