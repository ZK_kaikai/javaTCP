package youwen.callback;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.Map;
import java.util.Set;

import youwen.jiekou.TCPSercallBack;
import youwen.mysocket.TcpServerTest;






public class TCPServercallBack implements TCPSercallBack {

	private String Name = "TCPServercallBack";
	
	public TCPServercallBack(String Name) {
		this.Name = Name;
	}
	


	@Override
	public void Disconnected(SocketChannel SocketClients,String address) {
		System.out.println(Name+"TCP客户端"+SocketClients+"断开，地址为："+address);
		//TcpServerTest.TCPServer.Stop();
	}

	@Override
	public void MssageArrived(byte[] buffer, String address) {
		try {
			String mString = new String(buffer,"UTF-8");
			System.out.println("TCP数据到达..."+address+","+mString);
		} catch (UnsupportedEncodingException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		// TODO 自动生成的方法存根
	}

	@Override
	public void Hasconnect(SocketChannel SocketClient) {
		  System.out.println(Name+"TCP有新连接:");
		  System.out.println(SocketClient);
		  List<SocketChannel>  SocketClients = TcpServerTest.TCPServer.getSocketClients();
		 for(SocketChannel key:SocketClients) {
			 try {
				System.out.println("客户端："+key+",ReIP："+key.getRemoteAddress().toString()+",LoIP:"+key.getLocalAddress().toString());
			} catch (IOException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
				break;
			}
		 }
		// TODO 自动生成的方法存根
	}



	@Override
	public void IniResult(boolean rs, Integer port) {
		// TODO 自动生成的方法存根
		System.out.println("port:"+port+",TCPserver lisening...Reslut:"+rs);
	}
	
}
